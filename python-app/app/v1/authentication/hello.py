from datetime import datetime

from flask_restful import Resource, reqparse

from app.services.not_found import notFound

from app.scripts.test_script import testScript

class AuthenticationHello(Resource):
    def __init__(self, config, api_response):
        self.config = config
        self.startTime = datetime.now()

        self.api_response = api_response

        self.parser = reqparse.RequestParser()

    """
    @api {post} /auth/hello hello world
    @apiVersion 1.0.0
    @apiName post-hello
    @apiGroup AUTHENTICATION
    @apiPermission all
    @apiDescription show basic hello world
    """
    def get(self):
        self.parser.add_argument('test', type=str, location='args', default="false")
        data = self.parser.parse_args()

        testData = testScript(data)

        try:
            return self.api_response.success('hello', testData, startTime=self.startTime)
        except Exception as e:
            return self.api_response.failed('hello', str(e))
        

    def post(self):
        return notFound(self.api_response.failed)


    def put(self):
        return notFound(self.api_response.failed)


    def delete(self):
        return notFound(self.api_response.failed)

# ---------------------------------------------------------------------------------------------------------------------

def startHello(api, config, api_response):
    api.add_resource(AuthenticationHello, '/v1/auth/hello', endpoint = 'hello', resource_class_kwargs={'config': config, 'api_response': api_response})

import sys
sys.path.append('/application')

def testScript(params):
    print(params, '<-- pass params here')
    return ['hello', 'world']


# docker exec -ti flaskapi python app/scripts/test_script.py --params test=True
if __name__ == '__main__':
    import argparse
    from app.services.serialization import dumps

    parser = argparse.ArgumentParser(description='test scripts')
    parser.add_argument('--params', metavar='path', required=True, help='query params')
    args = parser.parse_args()

    currentParamsArray = args.params.split(',')
    params = {}
    if len(currentParamsArray) >= 1:
        for cpData in currentParamsArray:
            currentParamArray = cpData.split('=')
            if (len(currentParamArray) == 2):
                params[currentParamArray[0]] = currentParamArray[1]


    data = testScript(params)
    print('DATA: ', dumps(data))
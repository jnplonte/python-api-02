import os
import base64
import datetime

from flask import Flask, jsonify, g
from flask_restful import Resource, Api, abort, request

from app import config
from app.services.api_response import ApiResponse

from app.v1.authentication.hello import startHello

env = os.environ.get('ENV')
port = os.environ.get('PORT')

if env == 'development':
    configs = config.DevConfig
elif env == 'production':
    configs = config.ProdConfig
else:
    configs = config.LocalConfig

print('api environment is %s' % env.upper() if env is not None else 'local')
print('api listening to http://localhost:%s' % os.environ.get('PORT') if env is not None else '8383' )

# ---------------------------------------------------------------------------------------------------------------------

app = Flask(__name__)
api = Api(app)


@app.after_request
def corsCheck(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', '*')
    response.headers.add('Access-Control-Allow-Methods', '*')

    return response


@app.errorhandler(404)
def not_found(error):
    response = jsonify(api_response.failed('notfound', ''))
    response.status_code = 404

    return response


api_response = ApiResponse()

startHello(api, configs, api_response)

if __name__ == "__main__":
	app.run(debug=True)
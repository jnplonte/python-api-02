class BaseConfig(object):
    APP_NAME = 'pythonapi'
    APP_LOGO = 'https://via.placeholder.com/50'

    LOG_LEVEL = 'DEBUG'


class LocalConfig(BaseConfig):
    LOG_LEVEL = 'DEBUG'


class DevConfig(BaseConfig):
    LOG_LEVEL = 'DEBUG'


class ProdConfig(BaseConfig):
    LOG_LEVEL = 'INFO'
